from bluetooth import *
import threading
import sys
from time import gmtime, strftime, sleep

def advertise(clients):
    server_sock=BluetoothSocket( RFCOMM )
    server_sock.bind(("",PORT_ANY))
    server_sock.listen(1)

    port = server_sock.getsockname()[1]

    uuid = '94f39d29-7d6d-437d-973b-fba39e49d4ee'

    advertise_service( server_sock, 'BLT Host',
                       service_id = uuid,
                       service_classes = [ uuid, SERIAL_PORT_CLASS ],
                       profiles = [ SERIAL_PORT_PROFILE ],
    #                   protocols = [ OBEX_UUID ]
                        )

    print("Waiting for connection on RFCOMM channel %d" % port)

    while True:
        client_sock, client_info = server_sock.accept()
        print("Accepted connection from ", client_info)
        clients.append(client_sock)

def connect():
    addr = None

    print("Searching all nearby bluetooth devices for the BLT Host.")

    uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"
    service_matches = find_service( uuid = uuid, address = addr )

    if len(service_matches) == 0:
        print("Couldn't find the BLT Host =(")
        sys.exit(0)

    first_match = service_matches[0]
    port = first_match["port"]
    name = first_match["name"]
    host = first_match["host"]

    print("Connecting to \"%s\" on %s" % (name, host))

    # Create the client socket
    sock = BluetoothSocket( RFCOMM )
    sock.connect((host, port))

    return sock

def send():
    while True:
        sleep(1)
        print('send')

def recv():
    while True:
        sleep(1)
        print('recv')

if __name__ == '__main__':
    if sys.argv[1] == 'host':
        print('host')
        client_socks = []
        adv = threading.Thread(target=advertise, args=(client_socks,))
        adv.setDaemon(True)
        adv.start()

        while True:
            sleep(1)
            for sock in client_socks:
                data = sock.recv(1024)
                if len(data) != 0:
                    print(data)

    elif sys.argv[1] == 'client':
        print('client')
        sock = connect()

        while True:
            sleep(2)
            sock.send('from client with love')
